package com.xiaohuitalk.tools.exception;

/**
 * Created by xiaohuitalk on 2016/9/15.
 */
public class CocoException extends RuntimeException {

    /**
     * <p>
     * 应用异常构造方法
     * </p>
     *
     * @param message 消息
     */
    public CocoException(String message) {
        super(message);
    }

    /**
     * <p>
     * 应用异常构造方法
     * </p>
     *
     * @param message 消息
     * @param cause   原因
     */
    public CocoException(String message, Throwable cause) {
        super(message, cause);
    }

}
