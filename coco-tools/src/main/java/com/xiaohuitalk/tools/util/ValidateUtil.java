package com.xiaohuitalk.tools.util;

/**
 * Created by xiaohuitalk on 2016/9/15.
 */
public class ValidateUtil {

    private ValidateUtil() {

    }

    /**
     * 不为空验证
     */
    public static boolean required(Object[] params) {
        if (params.length > 0) {
            for (Object param : params) {
                if (StringUtil.isEmpty(param)) {
                    return false;
                }
            }
        }
        return true;
    }

}
