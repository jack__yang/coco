package com.xiaohuitalk.tools.util;

import java.util.Collection;

/**
 * Created by xiaohuitalk on 2016/9/15.
 */
public class CollectionUtil {

    private CollectionUtil() {

    }

    /**
     * 判断集合是否非空
     */
    public static boolean isNotEmpty(Collection<?> collection) {
        return !CollectionUtil.isEmpty(collection);
    }

    /**
     * 判断集合是否为空
     */
    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

}
