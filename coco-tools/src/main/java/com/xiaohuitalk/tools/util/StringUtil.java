package com.xiaohuitalk.tools.util;

/**
 * Created by xiaohuitalk on 2016/9/15.
 */
public class StringUtil {

    private StringUtil() {

    }

    public static boolean isEmpty(Object obj) {
        if (obj == null || "".equals(obj)) {
            return true;
        }
        return false;
    }

    public static boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

}
