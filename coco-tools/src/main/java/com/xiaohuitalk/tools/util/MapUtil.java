package com.xiaohuitalk.tools.util;

import java.util.Map;

/**
 * Created by xiaohuitalk on 2016/9/15.
 */
public class MapUtil {

    private MapUtil() {

    }

    /**
     * 判断集合是否非空
     */
    public static boolean isNotEmpty(Map<?, ?> map) {
        return !MapUtil.isEmpty(map);
    }

    /**
     * 判断集合是否为空
     */
    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

}
