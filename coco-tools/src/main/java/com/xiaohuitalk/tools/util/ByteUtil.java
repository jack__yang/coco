package com.xiaohuitalk.tools.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;

/**
 * Created by xiaohuitalk on 2016/9/15.
 */
public class ByteUtil {

    private ByteUtil()  {

    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ByteUtil.class);

    /**
     * 字节数组转字符串
     */
    public static String byteArrayToString(byte[] body) {
        String s = null;
        try {
            s = new String(body, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("== 转换失败", e);
        }
        return s;
    }

}
