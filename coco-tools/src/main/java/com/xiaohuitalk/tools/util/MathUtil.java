package com.xiaohuitalk.tools.util;

import java.math.BigDecimal;

/**
 * Created by xiaohuitalk on 2016/9/15.
 */
public class MathUtil {

    private MathUtil() {

    }

    /**
     * 四舍五入保留指定位数小数
     */
    public static double Double2Scale(Double v, int scale) {
        BigDecimal b = new BigDecimal(v);
        return b.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

}
